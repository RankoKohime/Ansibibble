ansible-laptop-tweaks
=====================

Performs various system tweaks specific to laptop hardware for Linux.

Requirements
------------

Linux, and some variety of ansible.  Some tweaks may require specific hardware to be usable.

Role Variables
--------------

TODO

Dependencies
------------

None

Example Playbook
----------------

TODO

License
-------

GPLv3

Author Information
------------------

Ranko Kohime < at runbox dat com >
