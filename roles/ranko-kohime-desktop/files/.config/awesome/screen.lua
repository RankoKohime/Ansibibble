local awful     = require("awful")                            -- Standard awesome library
local wibox     = require("wibox")                            -- Widget and layout library
local beautiful = require("beautiful")                        -- Theme handling library
local gears     = require("gears")                            -- Standard awesome library
--require("wallpaper")


local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)



awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)

    -- Each screen has its own tag table.
--    awful.tag({ "1", "2", "3" }, s, awful.layout.layouts[1])
    if os.getenv("DISPLAY") == ":0.2"
      then
        layouts = awful.layout.layouts
        tags= {
          settings = {
            { names  = { "Work", "Chat", "pavucontrol" },
              layout = { layouts[2], layouts[3], layouts[4] }
            }
          }
        }
        tags[s] = awful.tag(tags.settings[s.index].names, s, tags.settings[s.index].layout)
    elseif os.getenv("DISPLAY") == ":0.0"
      then
        layouts = awful.layout.layouts
        tags= {
          settings = {
            { names  = { "Internet 1", "Internet 2", "Internet 3", "Multimedia", "Podcast 1", "Podcast 2", "Stuff", "Conky" },
              layout = { layouts[9], layouts[9], layouts[9], layouts[5], layouts[3], layouts[3], layouts[3], layouts[3] }
            }
          }
        }
        tags[s] = awful.tag(tags.settings[s.index].names, s, tags.settings[s.index].layout)

    elseif os.getenv("DISPLAY") == ":0.1"
      then
        layouts = awful.layout.layouts
        tags= {
          settings = {
            { names  = { "Gaming", "Stuff", "Chat", "Coding", "Work", "KNR" },
              layout = { layouts[9], layouts[9], layouts[9], layouts[9], layouts[9], layouts[9] }
            }
          }
        }
        tags[s] = awful.tag(tags.settings[s.index].names, s, tags.settings[s.index].layout)

       else
         awful.tag({ "1", "2", "3" }, s, awful.layout.layouts[1])
    end

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist(s, awful.widget.taglist.filter.all, taglist_buttons)

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, tasklist_buttons)

    -- Create the wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            mylauncher,
            s.mytaglist,
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        { -- Right widgets
            layout = wibox.layout.fixed.horizontal,
            mykeyboardlayout,
            wibox.widget.systray(),
            mytextclock,
            s.mylayoutbox,
        },
    }
end)

