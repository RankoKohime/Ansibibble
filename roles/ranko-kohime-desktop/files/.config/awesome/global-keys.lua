local awful     = require("awful")                            -- Standard awesome library
local hotkeys_popup = require("awful.hotkeys_popup").widget   -- New in 4.0 Hotkey popup


globalkeys = awful.util.table.join(
--[[
-- Custom Keys
  -- Volume Keys
        -- Laptop Keys
    awful.key({ }, "XF86AudioRaiseVolume",
      function ()
        awful.util.spawn("amixer set Master 5%+", false) end),
    awful.key({ }, "XF86AudioLowerVolume",
      function ()
        awful.util.spawn("amixer set Master 5%-", false) end),
    awful.key({ }, "XF86AudioMute",
      function ()
        awful.util.spawn("amixer sset Master toggle", false) end),
  -- External Keyboard
    awful.key({ modkey,         }, "F4",
      function ()
        awful.util.spawn("amixer set Master 5%+", false) end),
    awful.key({ modkey,         }, "F3",
      function ()
        awful.util.spawn("amixer set Master 5%-", false) end),
    awful.key({ modkey          }, "F2",
      function ()
        awful.util.spawn("amixer sset Master toggle", false) end),
  -- Other Custom Keys
    awful.key({modkey,      }, "F5",
      function ()
        awful.util.spawn("~/.screenlayout/internal.sh", false) end),
    awful.key({modkey,      }, "F6",
      function ()
        awful.util.spawn("~/.screenlayout/hdmi-768p-underscan.sh", false) end),
    awful.key({modkey,      }, "F7",
      function ()
        awful.util.spawn("~/.screenlayout/hdmi-1080p-underscan.sh", false) end),
    awful.key({modkey,      }, "F8",
      function ()
        awful.util.spawn("~/.screenlayout/hdmi-1080p-monitor.sh", false) end),
    awful.key({modkey,      }, "l",
      function ()
        awful.util.spawn("xscreensaver-command -lock", false) end),
--]]

    awful.key({modkey,      }, "F9",
      function ()
        awful.util.spawn("/home/ranko/bin/audio-control.sh spkswap", false) end),

    awful.key({modkey,      }, "F10",
      function ()
        awful.util.spawn("/home/ranko/bin/audio-control.sh spkmute", false) end),

    awful.key({modkey,      }, "F11",
      function ()
        awful.util.spawn("/home/ranko/bin/audio-control.sh spkvldn", false) end),

    awful.key({modkey,      }, "F12",
      function ()
        awful.util.spawn("/home/ranko/bin/audio-control.sh spkvlup", false) end),

    awful.key({ modkey,     }, "Print",
      function ()
        awful.util.spawn("/home/ranko/bin/pg279q-rate-switch.sh toggle", false) end),

    awful.key({ modkey,     }, "Scroll_Lock",
      function ()
        awful.util.spawn("/home/ranko/bin/audio-control.sh miclkvl", false) end),

    awful.key({ modkey,     }, "Pause",
      function ()
        awful.util.spawn("/home/ranko/bin/audio-control.sh micmute", false) end),

-- all minimized clients are restored 
    awful.key({ modkey, "Shift"   }, "n", 
      function()
        local tag = awful.tag.selected()
        for i=1, #tag:clients() do
          tag:clients()[i].minimized=false
--          tag:clients()[i]:redraw()  -- Not working or needed in 4.0+?
        end
      end),

    awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
              {description="show help", group="awesome"}),
    awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
              {description = "view previous", group = "tag"}),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
              {description = "view next", group = "tag"}),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
              {description = "go back", group = "tag"}),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
        end,
        {description = "focus next by index", group = "client"}
    ),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
        end,
        {description = "focus previous by index", group = "client"}
    ),
    awful.key({ modkey,           }, "w", function () mymainmenu:show() end,
              {description = "show main menu", group = "awesome"}),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end,
              {description = "focus the next screen", group = "screen"}),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end,
              {description = "focus the previous screen", group = "screen"}),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
              {description = "jump to urgent client", group = "client"}),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end,
        {description = "go back", group = "client"}),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.spawn(terminal) end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ modkey, "Control" }, "r", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit,
              {description = "quit awesome", group = "awesome"}),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
              {description = "select next", group = "layout"}),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
              {description = "select previous", group = "layout"}),

    awful.key({ modkey, "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                      client.focus = c
                      c:raise()
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Prompt
    awful.key({ modkey },            "r",     function () awful.screen.focused().mypromptbox:run() end,
              {description = "run prompt", group = "launcher"}),

    awful.key({ modkey }, "x",
              function ()
                  awful.prompt.run {
                    prompt       = "Run Lua code: ",
                    textbox      = awful.screen.focused().mypromptbox.widget,
                    exe_callback = awful.util.eval,
                    history_path = awful.util.get_cache_dir() .. "/history_eval"
                  }
              end,
              {description = "lua execute prompt", group = "awesome"}),
    -- Menubar
    awful.key({ modkey }, "p", function() menubar.show() end,
              {description = "show the menubar", group = "launcher"})
)

