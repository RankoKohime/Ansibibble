local awful     = require("awful")               -- Standard awesome library
awful.rules     = require("awful.rules")         -- Standard awesome library

awful.rules.rules = {
  { rule =
      { class = "Audacity", name = "Apply Chain" },
    properties =
      { floating = true, ontop = true }
  },
  { rule =
      { class = "Clementine" },
    properties =
      { floating = true, ontop = true, sticky = true, skip_taskbar = true },
      callback = function( c )
      c:geometry( { x = 1718 , y = 33 , width = 1920 , height = 1080 } )
      end
  },
  { rule =
      { class = "gimp" },
    properties =
      { floating = true }
  },
  { rule =
      { class = "URxvt" },
    properties =
      { floating = true },
      callback = function( c )
      c:geometry( { width = 1808 , height = 1044 } )
      end
  }
}
