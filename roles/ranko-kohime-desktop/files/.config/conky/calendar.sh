#!/bin/bash
############################################################
# This work is licensed under the Creative Commons         #
# Attribution-Share Alike 3.0 Unported License.            #
# To view a copy of this license, visit                    #
# http://creativecommons.org/licenses/by-sa/3.0/           #
# or send a letter to Creative Commons, 171 Second Street, #
# Suite 300, San Francisco, California, 94105, USA.        #
############################################################

date=$(date '+%F')
DAY=${date:8:2}
# m="-m" # uncomment this line for starting the week on Monday instead of Sunday.
cal=$(cal $m)
prev=$(cal $m $(date '+%-m %Y' --date="${date:0:7}-15 -1 month")|sed 's/ *$//;/^$/d'|tail -1)
next=$(cal $m $(date '+%-m %Y' --date="${date:0:7}-15 +1 month")|sed '/^ *&/d;1,2d;s/^ *//'|head -1)
if [ ${#next} == 19 ] ;then next=$'\n'"\${color fcab03} $next"
else next="\${color fcab03}  $next"
fi
if [ ${#prev} == 20 ]; then prev="$prev"$'\n '
else prev="$prev  "
fi
current=$(echo "${cal:42}"|sed -e '/^ *$/d' -e 's/^/ /' -e 's/$/ /' -e 's/^ *1 / 1 /' )
current=$(echo "$current"|sed -e /" ${DAY/#0/} "/s/" ${DAY/#0/} "/" "'${color ff0000}'"${DAY/#0/}"'${color}'" "/ -e 's/^ //' -e 's/ *$//')
echo -e "\${color}${cal:0:21}${cal:21:21}\${color fcab03}$prev\${color}$current$next"
