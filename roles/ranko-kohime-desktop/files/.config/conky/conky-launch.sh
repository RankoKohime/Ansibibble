#!/bin/bash
export DISPLAY=:0.0
CONF=$HOME/.config/conky

# Main right-side Conky
conky -c $CONF/main.conf

# Calendar, upper right corner
#conky -c $CONF/calendar.conf

# Legacy readouts from Gkrellm
#gkrellm
