#!/bin/bash

cd ~/Downloads
git clone https://github.com/hirschmann/nbfc
sudo apt update; sudo apt install mono-complete


# After you have built NBFC, copy the files from nbfc/Linux/bin/ReleaseLinux/ to /opt/nbfc/
# To install the NBFC service, copy nbfc/Linux/nbfc.service and nbfc/Linux/nbfc-sleep.service into /etc/systemd/system/
# To enable and start the service, open a terminal and type systemctl enable nbfc --now
# (optional) Add /opt/nbfc/ to PATH, so you can call nbfc from anywhere
# Select a config: nbfc config --apply "name of a config file without extension"

