#!/bin/bash

if [ -z "$1" ]
then
    echo -e "Usage: $0 HOSTNAME"
    exit 1
else
    ansible -m setup -i $1, all
fi
