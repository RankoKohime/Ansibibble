#!/bin/bash

deployDesktopHosts=elli.ask,kara.ask,maia.ask,mimir.ask
deployServerHosts=alfheim.theatomicass.com,syn.ask,
askPass="--ask-pass"
askBecome="--ask-become-pass"
Verbosity=" "    # "-vvv"

# I need this because I am not a clevel man.
#                                                              And this is why  V
#ansible-playbook "$askPass" "$askBecome" "$Verbosity" -i "${deployDesktopHosts}," Deploy.yml


case $1 in
  desktop ) ansible-playbook "$askPass" "$askBecome" "$Verbosity" -i "${deployDesktopHosts}," Deploy_Desktop.yml ;;
  server  ) ansible-playbook "$askPass" "$askBecome" "$Verbosity" -i "${deployServerHosts}," Deploy_Server.yml;;
  *       ) echo -e "Valid options are desktop, server";;
esac
