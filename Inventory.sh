#!/bin/bash

INVENTORY_ADDRESS_SPACE="10.10.25.0/24"
INVENTORY_EXCLUDE="1.1.1.1/32"

ansible localhost -m community.general.nmap -a "\
  plugin=community.general.nmap \
  address=${INVENTORY_ADDRESS_SPACE} \
  exclude=${INVENTORY_EXCLUDE} \
  ipv4=true \
  ipv6=true \
  ports=false \
  "
